#include <stdio.h>
#include <jni.h>
#include <string.h>
#include <malloc.h>

const char KEY[] =  // G1tL4bV4ultK3y
        {0x47, 0x31, 0x74, 0x4c, 0x34, 0x62, 0x56, 0x34, 0x75, 0x6c, 0x74, 0x4b, 0x33, 0x79};
const char VAULT_PASSWORD[] = // [tanuki](58f3fab849a1e79009d069ed9f233798) XOR G1tL4bV4ultK3y
        {0x1c, 0x45, 0x15, 0x22, 0x41, 0x09, 0x3f, 0x69, 0x5d, 0x59, 0x4c, 0x2d,
         0x00, 0x1f, 0x26, 0x53, 0x4c, 0x78, 0x0d, 0x03, 0x67, 0x51, 0x42, 0x55,
         0x44, 0x7b, 0x0a, 0x1d, 0x77, 0x07, 0x4d, 0x29, 0x50, 0x5b, 0x30, 0x06,
         0x46, 0x5f, 0x43, 0x72, 0x0b, 0x50};
const int PASSWORD_LENGTH = 42;
const int KEY_LENGTH = 14;

char *decrypt(const char *toDecrypt) {
    char *output = malloc (PASSWORD_LENGTH);

    for (int i = 0; i < PASSWORD_LENGTH; i++) {
        output[i] = toDecrypt[i] ^ KEY[i % KEY_LENGTH];
    }

    return output;
}

JNIEXPORT jboolean JNICALL
Java_com_gitlab_contribute2020ctf_FlagVaultActivity_verifyPassword(
        JNIEnv *pEnv,
        jobject pThis,
        jstring pPassword) {
    jboolean success;
    if (pPassword == NULL) {
        success = JNI_FALSE;
    } else {
        const char *password = (*pEnv)->GetStringUTFChars(pEnv, pPassword, 0);
        const char *decrypted = decrypt(VAULT_PASSWORD);
        success = (jboolean) (strcmp(password, decrypted) == 0);
        (*pEnv)->ReleaseStringUTFChars(pEnv, pPassword, password);
    }
    return success;
}

