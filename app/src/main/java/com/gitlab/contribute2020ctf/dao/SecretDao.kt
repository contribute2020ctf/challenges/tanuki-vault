package com.gitlab.contribute2020ctf.dao

import androidx.room.Dao
import androidx.room.Query
import com.gitlab.contribute2020ctf.entities.Secret

@Dao
interface SecretDao {
    @Query("SELECT * FROM secret")
    fun getAll(): List<Secret>
}
