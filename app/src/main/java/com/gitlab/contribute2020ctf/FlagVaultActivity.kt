package com.gitlab.contribute2020ctf

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_flag_vault.*


class FlagVaultActivity : AppCompatActivity() {

    private external fun verifyPassword(password: String?): Boolean

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flag_vault)

        findViewById<Button>(R.id.button).setOnClickListener {
            val passwordField = findViewById<EditText>(R.id.editText)

            if (verifyPassword(passwordField.text.toString())) {
                textViewVault.text =
                    SecretDatabase
                        .getDatabase(applicationContext)
                        .secretDao()
                        .getAll()
                        .joinToString { secret ->
                            "${secret.value} (${secret.description})"
                        }
            } else {
                textViewVault.text = "Wrong password"
            }

            passwordField.text.clear()
        }
    }

    companion object {
        init {
            System.loadLibrary("vault")
        }
    }
}
