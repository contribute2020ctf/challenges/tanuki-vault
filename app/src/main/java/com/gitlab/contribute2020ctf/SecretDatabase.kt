package com.gitlab.contribute2020ctf

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.gitlab.contribute2020ctf.dao.SecretDao
import com.gitlab.contribute2020ctf.entities.Secret

@Database(entities = arrayOf(Secret::class), version = 1, exportSchema = false)
abstract class SecretDatabase : RoomDatabase() {
    abstract fun secretDao(): SecretDao

    companion object {
        @Volatile
        private var INSTANCE: SecretDatabase? = null

        fun getDatabase(applicationContext: Context): SecretDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance =
                    Room
                        .databaseBuilder(
                            applicationContext,
                            SecretDatabase::class.java,
                            "secrets_database"
                        )
                        .createFromAsset("secret.db")
                        .allowMainThreadQueries() // This is bad, but hey it's a CTF not a production app :D
                        .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
