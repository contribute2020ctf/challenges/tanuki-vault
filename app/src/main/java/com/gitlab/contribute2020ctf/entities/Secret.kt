package com.gitlab.contribute2020ctf.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Secret(
    @PrimaryKey val uid: Int,
    @ColumnInfo(name = "value") val value: String?,
    @ColumnInfo(name = "description") val description: String?
)
